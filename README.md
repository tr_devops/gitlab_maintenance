# GitLab_Maintenance

## Use

**Required variables**:
    
1. PROJECTID = ID of the project you want to clean the tags up 
2. KEEP = The number of tags (latest) you would like to keep
3. RUN_TYPE = The type of run you want to start. 
        DRY = nothing will be deleted and list of tags that would be removed is returned
        DEL = the command will be run and the appropriate tags will be deleted
