import requests
import argparse

def getDatafromApi(url, token):
    res_x = {}
    npg = 0
    header = {
        "Private-Token": token
    }
    try:
        res = requests.get(url, headers=header)    
        res_x = res.json()
        if 'X-Next-Page' in res.headers:
            npg = res.headers['X-Next-Page']
        else:
            npg = None
    except Exception as e:
        print(e)
        exit(1)
    return res_x, npg

def deleteDatafromApi(url, token):
    res = {}
    header = {
        "Private-Token": token
    }
    try:
        res = requests.delete(url, headers=header)
    except Exception as e:
        print(e)
        exit(1)
    return res

def get_tag_list(projId, token):
    tag_list = []
    url = "https://gitlab.com/api/v4/projects/{0}/repository/tags".format(projId)
    res, npg = getDatafromApi(url, token)
    for row in res:
        tag_list.append(row['name'])
    return tag_list

def delete_tag(projId, tagNm, token):
    url = "https://gitlab.com/api/v4/projects/{0}/repository/tags/{1}".format(projId, tagNm)
    res = deleteDatafromApi(url, token)
    return res

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-accesstoken", help="The GitLab access token to create the new token.", required=True)
    parser.add_argument("-projectid", help="The GitLab Project ID.", type=int, required=True)
    parser.add_argument("-keep", help="The number of repository tags to keep.", type=int, required=True)
    parser.add_argument("run", help="Type of run. Choices are: DEL, DRY", choices=['DEL','DRY'])
    args = parser.parse_args()

    if args.run == "DRY":
        tag_list = get_tag_list(args.projectid, args.accesstoken)
        keep = args.keep
        tag_list = tag_list[keep:]
        print(f"The following tags on ProjectId: {args.projectid} would be DELETED:")
        for tag in tag_list:
            print(tag)
    elif args.run == "DEL":
        tag_list = get_tag_list(args.projectid, args.accesstoken)
        keep = args.keep
        tag_list = tag_list[keep:]
        for tag in tag_list:
            response = delete_tag(args.projectid, tag, args.accesstoken)
            print(f"ProjectID: {args.projectid}  Tag: {tag} has been deleted. - Response: {response.status_code}")
    else:
        print("Unknown command issued.")
